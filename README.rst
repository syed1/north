#####
North
#####

North is an android application targetted for treking sites 
in India.

==================
GPX data to Sqlite 
==================

the `tools` folder of `north` consists of code which 
takes a `gpx` file and appends its route info to a sqlite database
which already has the map which image tiles as binary blobs.::

    (north)syedah@syedah-Latitude-E6410: ~/prog/north/tools $ python embed_gpx.py  --help
    usage: embed_gpx.py [-h] [--gpx GPX] [--db DB]
    
    script which embeds a GPX file in an Rmaps compatible sqlite
    
    arguments:
      -h, --help  show this help message and exit
      --gpx GPX   gpx file which has track info
      --db DB     sqlite file which has track tiles


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Setting up python for embed_gpx.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
uses `gpxpy <http://www.trackprofiler.com/gpxpy/index.html>`_::
    
    pip install gpxpy

    import gpxpy.parser as parser

    gpx_file = open( 'test_files/cerknicko-jezero.gpx', 'r' )

    gpx_parser = parser.GPXParser( gpx_file )
    gpx_parser.parse()

    gpx_file.close()

    gpx = gpx_parser.get_gpx()

    for route in gpx.routes:
        print 'Route:'
            for point in route:
                    print 'Point at ({0},{1}) -> {2}'.format( point.latitude, point.longitude, point.elevation )




==========
References
==========

* GPX documenation `here <http://www.topografix.com/gpx_for_users.asp>`_
* Android `SQLite Asset helper  <https://github.com/jgilfelt/android-sqlite-asset-helper>`_
* Map tiles `file naming conventions <http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames>`_
* Other bugs/code related to overlay
    * `this <http://code.google.com/p/osmdroid/issues/detail?id=138>`_ gives some info on setting zoom level and tile source
    * `file archive tile provider <http://android.martinpearman.co.uk/b4a/osmdroid/documentation/native_android_library/org/osmdroid/tileprovider/modules/MapTileFileArchiveProvider.html>`_ which will supply tiles from file ? `Source <http://code.google.com/p/osmdroid/source/browse/trunk/osmdroid-android/src/org/osmdroid/tileprovider/modules/MapTileFileArchiveProvider.java?r=681>`_
    * `basic map tile <http://android.martinpearman.co.uk/b4a/osmdroid/documentation/native_android_library/org/osmdroid/tileprovider/MapTileProviderBasic.html>`_ provider and `this  <http://www.androidadb.com/class/ma/MapTileProviderBasic.html>`_
    * Good `tutorial on osmdroid <http://mappingdev.wordpress.com/tag/offline/>`_



