package com.example.treking.india;


import java.io.InputStream;

import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.ExpirableBitmapDrawable;
import org.osmdroid.tileprovider.MapTile;
import org.osmdroid.tileprovider.tilesource.BitmapTileSourceBase.LowMemoryException;
import org.osmdroid.tileprovider.tilesource.ITileSource;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.util.Log;



public class TileSource implements ITileSource {
	

	private static final int ZOOM_MAXLEVEL = 20;
	private static final int ZOOM_MINLEVEL = 12;
	private MapDatabase db;
	
	public TileSource(Context ctx, MapDatabase db_tiles) throws SQLiteException {
		super();
		db = new MapDatabase(ctx);
	}
	
	public Drawable getTile(final int x, final int y, final int z) {
		Log.d("getTile(int,int,int)", " Asking for " + Integer.toString(x) + "  "+ Integer.toString(y) + "  " + Integer.toString(z));
		return db.getTile(x, y, z);
	}

	
	@Override
	public Drawable getDrawable(String arg) throws LowMemoryException {
		Log.d("getDrawable","Calling drawable of our source " + arg);
		// TODO Auto-generated method stub
		String[] res = arg.split("/");
		return this.getTile(Integer.parseInt(res[1]), Integer.parseInt(res[2]), Integer.parseInt(res[0]));
		/*ShapeDrawable mDrawable = new ShapeDrawable(new RectShape());
        mDrawable.getPaint().setColor(0xff74AC23);
		return mDrawable;*/
		
	}

	@Override
	public Drawable getDrawable(InputStream arg0) throws LowMemoryException {
		Log.d("getDrawable(InputStream)","Calling drawable of our source ( with imput stream )");
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int getMaximumZoomLevel() {
		// TODO Auto-generated method stub
		return ZOOM_MAXLEVEL;
	}

	@Override
	public int getMinimumZoomLevel() {
		// TODO Auto-generated method stub
		return ZOOM_MINLEVEL;
	}

	@Override
	public String getTileRelativeFilenameString(MapTile aTile) {
		// TODO Auto-generated method stub
		String tile_path =  "sqlite://" + aTile.getZoomLevel() + "/" + aTile.getX() + "/" + aTile.getY()
				+ ".png";
		
		Log.d("getTileRelativeFilenameString",tile_path);
		return tile_path;

	}

	@Override
	public int getTileSizePixels() {
		// TODO Auto-generated method stub
		return 256;
	}

	@Override
	public String localizedName(ResourceProxy arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int ordinal() {
		// TODO Auto-generated method stub
		return 0;
	}

}