package com.example.treking.india;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.ResourceProxy.bitmap;
import org.osmdroid.ResourceProxy.string;
import org.osmdroid.events.MapListener;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MyLocationOverlay;
import org.osmdroid.views.overlay.PathOverlay;
import org.osmdroid.views.overlay.SimpleLocationOverlay;
import org.osmdroid.views.overlay.TilesOverlay;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class MainActivity extends Activity {

	private BoundedMapView mOsmv;
	
	private TilesOverlay mTilesOverlay;
	private MapController mapController;
	private DbTileProvider mTileProvider;
	private TileSource mTileSource;
	private ResourceProxy pResourceProxy;

	private MapDatabase db;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		//this.mOsmv = new MapView(this, 256);
		db = new MapDatabase(this);
		pResourceProxy = new MapResourceProxy();
		
		 /* Tiles */
		Log.i("MainActivity", "Adding tiles");
		try {
			
			mTileSource = new TileSource(this.getBaseContext(),db);
			mTileProvider = new DbTileProvider(this.getBaseContext(),mTileSource);
			
			mTilesOverlay = new TilesOverlay(mTileProvider, getBaseContext());
			mTilesOverlay.setUseDataConnection(false);
			
			
			//this.mOsmv.invalidate();
			
		 } catch (SQLiteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }

		
		
		/* Track */
		 PathOverlay myPath = new PathOverlay(Color.RED, this);
		
		 Cursor points = db.getPoints();
		 
		 int size = points.getCount();
		 for(int i=0;i<size;i++)
		 {
			 float lat = points.getFloat(points.getColumnIndex("lat"));
			 float lng = points.getFloat(points.getColumnIndex("lng"));
			 GeoPoint pt = new GeoPoint(lat,lng);
			 myPath.addPoint(pt);
			 points.moveToNext();
		 }
		 points.close();
	 
		
		 
		 /*Compass and location tracking */
		 //MyLocationOverlay mLocationOverlay = new MyLocationOverlay(getBaseContext(), mOsmv );
		 
		 //mLocationOverlay.enableCompass();
		 //mLocationOverlay.enableMyLocation();
		 //mLocationOverlay.enableFollowLocation();
		 //mLocationOverlay.disableMyLocation();
		 //mLocationOverlay.disableFollowLocation();
		 //mLocationOverlay.disableCompass();
		 //mLocationOverlay.setEnabled(true);
		 
		
		 
		 /* Map View */ 
		 mOsmv = new BoundedMapView(getBaseContext(), pResourceProxy, mTileProvider);
		 this.mOsmv.setBuiltInZoomControls(true);
		 this.mOsmv.setMultiTouchControls(true);
		 
		 
		 BoundingBoxE6 limits = db.getMapBounds();
		 this.mOsmv.setScrollableAreaLimit(limits);
		 
		 this.mOsmv.getOverlays().add(mTilesOverlay);
		 //this.mOsmv.getOverlays().add(mLocationOverlay);
		 this.mOsmv.getOverlays().add(myPath);
		 
		 mapController = this.mOsmv.getController();
		 
		 
		 
 		 GeoPoint pt1= new GeoPoint(12.917550, 75.504530);
		 mapController.setZoom(12);
		 mapController.setCenter(pt1);
		 
		 final RelativeLayout rl = new RelativeLayout(this);
		 rl.addView(this.mOsmv, new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT,
				 LayoutParams.FILL_PARENT));
		 setContentView(rl);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
