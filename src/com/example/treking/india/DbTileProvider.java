package com.example.treking.india;


import org.osmdroid.tileprovider.ExpirableBitmapDrawable;
import org.osmdroid.tileprovider.MapTile;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.tileprovider.tilesource.BitmapTileSourceBase.LowMemoryException;
import org.osmdroid.tileprovider.tilesource.ITileSource;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.app.ActivityManager;



import android.os.Environment;
import android.support.v4.util.LruCache;
import android.util.Log;


public class DbTileProvider extends MapTileProviderBase {
	
	private boolean mSdCardAvailable = true;
	private ITileSource mTileSource;
	private LruCache<MapTile, Drawable> mTileCache;
	private final Drawable mEmptyTile;
	

	public DbTileProvider(Context context, TileSource pTileSource) {
		super(pTileSource);
		
		checkSdCard();
		final int memClass = ((ActivityManager) context.getSystemService(
	            Context.ACTIVITY_SERVICE)).getMemoryClass();
		final int cacheSize = 1024 * 1024 * memClass / 16;
		
		
		mTileSource = pTileSource;
		mTileCache = new LruCache<MapTile, Drawable>(cacheSize);
		Bitmap TempBmap = Bitmap.createBitmap(256, 256, Config.ARGB_8888);
		for ( int i = 0 ; i< TempBmap.getHeight(); i++)
			for( int j = 0 ; j < TempBmap.getWidth();j++)
				TempBmap.setPixel(i, j, Color.BLACK);
		
		mEmptyTile = new ExpirableBitmapDrawable(TempBmap);
		

		final IntentFilter mediaFilter = new IntentFilter();
		mediaFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
		mediaFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
		mediaFilter.addDataScheme("file");
	}

	protected void onMediaMounted() {
		// Do nothing by default. Override to handle.
	}

	protected void onMediaUnmounted() {
		// Do nothing by default. Override to handle.
	}


	@Override
	public int getMaximumZoomLevel() {
		// TODO Auto-generated method stub
		return mTileSource != null ? mTileSource.getMaximumZoomLevel() : MAXIMUM_ZOOMLEVEL;
	}

	@Override
	public int getMinimumZoomLevel() {
		// TODO Auto-generated method stub
		return mTileSource != null ? mTileSource.getMinimumZoomLevel() : MINIMUM_ZOOMLEVEL;
	}


	protected String getName() {
		// TODO Auto-generated method stub
		return "Database tile provider";
	}



	public boolean getUsesDataConnection() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setTileSource(ITileSource arg0) {
		mTileSource = arg0;
		// TODO Auto-generated method stub
		
	}

	
	private void checkSdCard() {
		final String state = Environment.getExternalStorageState();
		Log.i("DbTileProvider" , "sdcard state: " + state);
		mSdCardAvailable = Environment.MEDIA_MOUNTED.equals(state);
	}

	
	
	
	// ===========================================================
		// Inner and Anonymous Classes
		// ===========================================================
	
	protected boolean getSdCardAvailable() {
		return mSdCardAvailable;
	}

		@Override
		public Drawable getMapTile(MapTile pTile) {
			final String arg = Integer.toString(pTile.getZoomLevel()) + "/" +
					Integer.toString(pTile.getX()) + "/" +
					Integer.toString(pTile.getY());
			
			Log.d("getMapTile", "Asking to get map tile " + arg);
			try {
				
				Drawable drawable;
					 if ( (drawable = mTileCache.get(pTile)) != null )
					 {
						 Log.i("getMapTile","Found tile in cache");
						 return drawable;
					 }
				drawable = mTileSource.getDrawable(arg);
				if ( drawable == null)
					mTileCache.put(pTile, mEmptyTile);
				else	
					mTileCache.put(pTile, drawable);
				
				return drawable;
			} catch (LowMemoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (OutOfMemoryError e) {
				e.printStackTrace();
				mTileCache.evictAll();
				System.gc();
			}
			
			// TODO Auto-generated method stub
			return null;
		}

	@Override
	public void detach() {
		// TODO Auto-generated method stub
		
	}
}
