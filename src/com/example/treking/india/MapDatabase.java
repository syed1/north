package com.example.treking.india;

import org.osmdroid.tileprovider.ExpirableBitmapDrawable;
import org.osmdroid.util.BoundingBoxE6;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MapDatabase extends SQLiteAssetHelper {
	
	private static final String DATABASE_NAME = "testdb";
	private static final int DATABASE_VERSION = 1;
	private static final int ZOOM_DIFF = 19;

	
	public MapDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

	
		// you can use an alternate constructor to specify a database location
		// (such as a folder on the sd card)
		// you must ensure that this folder is available and you have permission
		// to write to it
		//super(context, DATABASE_NAME, context.getExternalFilesDir(null).getAbsolutePath(), null, DATABASE_VERSION);
	}
	
	public Cursor getPoints() {
		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		
		String [] sqlSelect = {"lat", "lng" };
		String sqlTables = "track_data";
		
		qb.setTables(sqlTables);
		Cursor c = qb.query(db, sqlSelect, null, null,
		null, null, null);
		
		c.moveToFirst();
		return c;
	}
	public Drawable getTile(int x,int y,int z)
	{
		SQLiteDatabase db = getReadableDatabase();

		String[] args= { Integer.toString(x) , Integer.toString(y), Integer.toString(17 - z) };
		
		String q = "SELECT image FROM tiles where x=? AND y=? AND z=?" + "(" + 
				" " +Integer.toString(x) + 
				" " +Integer.toString(y) + 
				" " +Integer.toString(ZOOM_DIFF - z) + ")";
		Log.i("DB" , q);
		Cursor c = db.rawQuery("SELECT image FROM tiles where x=? AND y=? AND z=?" , args);
		c.moveToFirst();

		
		
		if ( c.getCount() <= 0 )
		{
			Log.i("getTileDB" , "Tile not found");
			return null;
		}
		StringBuilder builder = new StringBuilder();
		for(String s : c.getColumnNames()) {
		    builder.append(s);
		}

		Log.i("SelectTile" , builder.toString() + " index "  + c.getColumnIndex("image"));
		byte[] data = null;
		//data = c.getBlob(c.getColumnIndex("image")); 
		data = c.getBlob(0);
		c.close();
		
		Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
		
		return new ExpirableBitmapDrawable(bmp);
	}
	
	public BoundingBoxE6 getMapBounds() {
		
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("SELECT MAX(lat) FROM track_data", null);
		c.moveToFirst();
		float north = c.getFloat(0);
		c.close();
		
		c = db.rawQuery("SELECT MIN(lat) FROM track_data", null);
		c.moveToFirst();
		float south = c.getFloat(0);
		c.close();
	

		c = db.rawQuery("SELECT MAX(lng) FROM track_data", null);
		c.moveToFirst();
		float east = c.getFloat(0) ;
		c.close();
		
		c = db.rawQuery("SELECT MIN(lng) FROM track_data", null);
		c.moveToFirst();
		float west = c.getFloat(0) ;
		c.close();

		Log.i("getLimits" , "North:" + Float.toString(north) +
				" South:" + Float.toString(south) +
				" East:" + Float.toString(east) +
				" West:" + Float.toString(west) );
		
		return new BoundingBoxE6( north,east,south,west);
	}
}