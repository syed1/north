Mobile Atlas
Mobile Atlas Creator (formerly known as TrekBuddy Atlas Creator) is an open source (GPL) program which creates offline atlases for GPS handhelds and cell phone applications

download from http://mobac.sourceforge.net/
add following lines to setting.xml for getting Google Map's Satellite images in Mobac

    <customMapSources>
        <customMapSource>
            <name>Custom Google Satellit</name>
            <minZoom>0</minZoom>
            <maxZoom>18</maxZoom>
            <tileType>jpeg</tileType>
            <tileUpdate>None</tileUpdate>
            <url>https://khms0.google.com/kh/v=115&amp;src=app&amp;x={$x}&amp;y={$y}&amp;z={$z}&amp;s=</url>
            <backgroundColor>#000000</backgroundColor>
        </customMapSource>
    </customMapSources>
