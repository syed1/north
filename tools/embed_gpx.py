#!/usr/bin/python

# script which embeds a GPX file in an Rmaps compatible 
# sqlite which is read by north

import gpxpy.parser as parser
import argparse
import sqlite3

arg_parser = argparse.ArgumentParser(description="script which embeds a GPX file in an Rmaps compatible sqlite")

arg_parser.add_argument('--gpx', help='gpx file which has track info')
arg_parser.add_argument('--db',  help='sqlite file which has track tiles')

args = arg_parser.parse_args()

gpx_file  = open(args.gpx)
gpx_parser = parser.GPXParser(gpx_file)
gpx_parser.parse()
gpx_file.close()

gpx = gpx_parser.get_gpx()

max_duration = 0 
max_segment = None
for track in gpx.tracks:
    print "Track duration %d " % (track.get_duration())
    for segment in track.segments : 
        print "Segment duration %d " % (segment.get_duration())
        if segment.get_duration() > max_duration : 
            max_duration = segment.get_duration()
            max_segment =  segment 


if len(max_segment.points) <= 0 : 
    raise Exception("No tracks found")

DROP_TABLE_QUERY="DROP TABLE if EXISTS track_data"
CREATE_TABLE_QUERY="CREATE TABLE if not EXISTS track_data ( lat FLOAT, lng FLOAT )"
CLEAR_TABLE="DELETE FROM track_data"
INSERT_QUERY="INSERT INTO track_data VALUES( %f , %f )"

conn = sqlite3.connect(args.db)
cur = conn.cursor()

cur.execute(DROP_TABLE_QUERY)
cur.execute(CREATE_TABLE_QUERY)
cur.execute(CLEAR_TABLE)
conn.commit()

for pt in max_segment.points :
    print " Lat: %f Lng: %f Ele: %f" % ( pt.latitude,pt.longitude,pt.elevation)
    cur.execute(INSERT_QUERY % ( pt.latitude,pt.longitude))

conn.commit()
conn.close()

